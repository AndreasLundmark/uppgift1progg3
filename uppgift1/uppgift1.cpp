// uppgift1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "Unit.hpp"
#include <vld.h>
#include <list>

namespace linkedlist
{
	template <class T>
	class LinkedList
	{
		struct Node // one node
		{
			T val; // the value in the node
			Node* next = NULL; // pointer to the next node (node pointer)

		};
		//node pointers created when list is created
		Node* n; //node
		Node* t; //temporary
		Node* h = NULL; //head
		Node* e; // end node

	public:

		void push_front(const T& value); // add value to the front of the list
		void push_back(const T& value); // add value to the back of the list
		void pop_front(); // remove value to the front of the list
		void pop_back(); // remove value to the back of the list
		int find_value(int value); // find value in list
		int find_value_at_position(int position); // shows the value at a selected node
		void clear_list(); // clear the whole list
		int size_of_list(); // clear the whole list
		void print_list() const;
	private:
	};

	template<class T>
	void LinkedList<T>::push_front(const T & value)
	{
		if (h == NULL)
		{
			std::cout << "Start Building List: " << std::endl;
			n = new Node; // point n to a newly created node 
			n->val = value; // assign value to node
			t = n; //place our temporary node pointer to node
			h = n; // place head node pointer at node
			e = n; // end node
		}
		else
		{
			n = new Node; // point n to a newly created node 
			n->val = value; // assign value to node
			t = h; // set t to h Node , the old h node
			h = n; // set h to the new node because it is in the front
			h->next = t; // link the new h node to the old one
			t = e; // move t to the end
		}
	}

	template<class T>
	void LinkedList<T>::push_back(const T & value) // add value to the back of the list
	{
		if (h == NULL)
		{
			std::cout << "Start Building List: " << std::endl;
			n = new Node; // point n to a newly created node 
			n->val = value; // assign value to node
			t = n; //place our temporary node pointer to node
			h = n; // place head node pointer at node
			e = n; // end node
				   //std::cout << n->val<< std::endl;
		}

		else
		{
			n = new Node; // point n to a newly created node 
			n->val = value;// assign value to node
			t->next = n; //make next point to the new node
			t = t->next; // move t to the new node
			e = n; // set end to the new node
		}
	}

	template<class T>
	void LinkedList<T>::pop_front()
	{
		Node* delete_pointer = NULL; // create a pointer to delete a node
		delete_pointer = h;// set deletpointer to the first node
		h = h->next; // move h one step ahead
		std::cout << "" << std::endl;
		std::cout << "Delete value: " << delete_pointer->val << " from list." << std::endl; // print the value in the node
		delete delete_pointer;
	}

	template<class T>
	void LinkedList<T>::pop_back()
	{
		Node* delete_pointer = NULL; // create a pointer to delete a node
		delete_pointer = e; // set deletpointer to the last node
		t = h; // place t at start to prepare for next while loop
		while (t->next != e) // place t before e
		{
			t = t->next;
		}
		e = t; // place e at the node before it
		e->next = NULL; // set end node next pointer to NULL
		std::cout << "" << std::endl;
		std::cout << "Delete value: " << delete_pointer->val << " from list." << std::endl; // print the value in the node
		delete delete_pointer;
	}

	template<class T>
	int LinkedList<T>::find_value(int value)
	{
		const Node* t = h;
		int count = 0;
		while (t != NULL)
		{
			if (t->val == value)
			{
				std::cout << "" << std::endl;
				std::cout << "found value: " << t->val << " at position: " << count + 1 << std::endl;
				return t->val;
			}

			count++;
			t = t->next;
		}
	}

	template<class T>
	int LinkedList<T>::find_value_at_position(int position) // shows the value at a selected node
	{
		t = h;
		for (int i = 0; i <= position; i++)
		{
			if (t != NULL)
			{
				if (i == position) // if it finds the position, print the text with the value
				{
					std::cout << "" << std::endl;
					std::cout << "The value is " << t->val << " at position " << position + 1 << " in the list. " << std::endl;
					return t->val;
					//std::cout << t->val << std::endl;
				}
				t = t->next;
			}
			else if (t == NULL) // if the node is NULL (if it have no value)
			{
				std::cout << "No value was found at position: " << position + 1 << ". the list starts at 0, can it be that you wanted the value from another position?" << std::endl;
			}
		}
	}

	template<class T>
	void LinkedList<T>::clear_list() // inte klar!!!
	{
		t = h;
		while (t != NULL)
		{
			Node* delete_pointer = NULL; // create a pointer to delete a node
			delete_pointer = h;// set deletpointer to the first node
			if(h->next != NULL) // as long as there is a next node
			h = h->next; // move h one step ahead
			else if(h->next == NULL) // if we are at the end
			{
				// set the pointers to NULL exept the delete pointer
				n = NULL; 
				t = NULL; 
				h = NULL; 
				e = NULL; 
				std::cout << "" << std::endl;
				std::cout << "Delete list..." << std::endl; // print the value in the node
			}
			delete delete_pointer;
		}
	}

	template<class T>
	int LinkedList<T>::size_of_list()
	{
		const Node* t = h; // move t to the h
		int count = 0;
		while (t != NULL) // move thru the list until the t == NULL
		{
			count++;
			t = t->next; // move t to the next node
		}
		std::cout << "" << std::endl;
		std::cout << "Size of list is: " << count << std::endl; // print the value in the node
		return count;
	}

	template<class T>
	void LinkedList<T>::print_list() const
	{
		const Node* t = h; // move t to the h
		std::cout << "" << std::endl;
		std::cout << "The List: " << std::endl;
		while (t != NULL) // move thru the list until the t == NULL
		{
			std::cout << t->val << std::endl; // print the value in the node
			t = t->next; // move t to the next node
		}
		if (h == NULL) // if the node is NULL (if it have no value)
		{
			std::cout << "" << std::endl;
			std::cout << "The list is empty!" << std::endl;
		}
	}	

}

namespace binarysearchtree
{
	template <class T>
	class BinarySearchTree
	{
		struct BNode // one node
		{
			T val; // the value in the node
			BNode* left = NULL; //child left
			BNode* right = NULL; //child right

		};
		//node pointers created when list is created
		BNode* n; //node
		BNode* t; //temporary
		BNode* h = NULL; //head

	public:

		void add_new(const T& value); // Create the tree if none exist, else create new node
		void choose_path(const T& value, BNode* ptr); // place the value at the position
		void create_new(const T& value); // add value to the tree
		void traversal_pre_order_run(BNode* ptr); // 
		void traversal_pre_order(); // 
		void traversal_in_order_run(BNode* ptr); // 
		void traversal_in_order(); // 
		void traversal_post_order_run(BNode* ptr); // 
		void traversal_post_order(); // 
		int find_value_run(int value, BNode* ptr); // find value in tree
		int find_value(int value); // find value in tree
		void clear_tree_run(BNode* ptr); // clear the whole tree
		void clear_tree(); // clear the whole tree
		int size_tree_run(BNode* ptr); // 
		int size_tree(); //
		int size_count;
	private:
	};

	template<class T>
	void BinarySearchTree<T>::add_new(const T & value)
	{
		if (h == NULL) // if the tree is empty, create a node and move h to it
		{
			std::cout << "Start Building tree: " << std::endl;
			std::cout << " " << std::endl;
			n = new BNode; // point n to a newly created node 
			n->val = value; // assign value to node
			t = n; //place our temporary node pointer to node
			h = n; // place head node pointer at node
		}
		else // if the tree is created, start searcj�hing for where to place the new value
		{
			choose_path(value, h);
		}
	}

	template<class T>
	void BinarySearchTree<T>::choose_path(const T& value, BNode* ptr)
	{
		if (value < ptr->val) // if value is less that the value of the node we are looking at
		{
			if (ptr->left != NULL)
			{
				choose_path(value, ptr->left);
			}
			else if(ptr->left == NULL)
			{
				n = new BNode; // point n to a newly created node 
				n->val = value; // assign value to node
				ptr->left = n;
				//std::cout << "create left" << std::endl;
			}
		}
		else if (value > ptr->val) // if value is greater that the value of the node we are looking at
		{
			if (ptr->right != NULL)
			{
				choose_path(value, ptr->right);
			}
			else if(ptr->right == NULL)
			{
				n = new BNode; // point n to a newly created node 
				n->val = value; // assign value to node
				ptr->right = n;
				//std::cout << "create right" << std::endl;
			}
		}
		else
		{
			std::cout << "The value" << value << " has already been added before! The value was not added again." << std::endl;
		}
	}

	template<class T>
	void BinarySearchTree<T>::create_new(const T& value)
	{
		n = new BNode; // point n to a newly created node 
		n->val = value; // assign value to node
	}

	template<class T>
	void BinarySearchTree<T>::traversal_pre_order_run(BNode* ptr) // v�nster sen h�ger gren (header, left subtree, right subtree)
	{
		if (h != NULL)
		{	
			std::cout << ptr->val << std::endl;
			if (ptr->left != NULL)
			{
				traversal_pre_order_run(ptr->left);
			}
			if (ptr->right != NULL)
			{
				traversal_pre_order_run(ptr->right);
			}
		}
		else
		{
			std::cout << " The tree is empty! " << std::endl;
		}
	}

	template<class T>
	void BinarySearchTree<T>::traversal_pre_order()
	{
		std::cout << " " << std::endl;
		std::cout << "Traversal_pre_order: " << std::endl;
		traversal_pre_order_run(h);
	}

	template<class T>
	void BinarySearchTree<T>::traversal_in_order_run(BNode* ptr) // minsta till h�gsta (left subtree, header, right subtree)
	{
		if (h != NULL)
		{
			if (ptr->left != NULL)
			{
				traversal_in_order_run(ptr->left);
			}
			std::cout << ptr->val<< std::endl;
			if (ptr->right != NULL)
			{
				traversal_in_order_run(ptr->right);
			}
		}
		else
		{
			std::cout << " The tree is empty! " << std::endl;
		}

	}

	template<class T>
	void BinarySearchTree<T>::traversal_in_order()
	{
		std::cout << " " << std::endl;
		std::cout << "Traversal_in_order: " << std::endl;
		traversal_in_order_run(h);
		

	}

	template<class T>
	void BinarySearchTree<T>::traversal_post_order_run(BNode* ptr) //minsta till h�gsta i v�nster, sen h�ger o sist header (left subtree, right subtree ,header)
	{
		if (h != NULL)
		{
			if (ptr->left != NULL)
			{
				traversal_in_order_run(ptr->left);
			}

			if (ptr->right != NULL)
			{
				traversal_in_order_run(ptr->right);
			}
			std::cout << ptr->val << std::endl;
		}
		else
		{
			std::cout << " The tree is empty! " << std::endl;
		}
	}

	template<class T>
	void BinarySearchTree<T>::traversal_post_order()
	{
		std::cout << " " << std::endl;
		std::cout << "Traversal_post_order: " << std::endl;
		traversal_post_order_run(h);
	}

	template<class T>
	int BinarySearchTree<T>::find_value_run(int value, BNode* ptr)
	{
		if (h != NULL)
		{
			if (ptr->left != NULL)
			{
				find_value_run(value, ptr->left);
			}

			if (ptr->right != NULL)
			{
				find_value_run(value, ptr->right);
			}
			if (ptr->val == value)
			{
				//std::cout << "The value: " << ptr->val << " was found in the tree." << std::endl;
				return ptr->val;
			}
			else if (ptr->val != value  &&  ptr == h)
			{
				//std::cout << "The value: " << value << " was not found in the tree." << std::endl;
			}
		}
		else
		{
			std::cout << " The tree is empty! You can not search for a value. " << std::endl;
		}
	}

	template<class T>
	int BinarySearchTree<T>::find_value(int value)
	{
		std::cout << " " << std::endl;
		std::cout << "Find_value: " << value << "."<< std::endl;
	
		find_value_run(value, h);
		return value;
	}

	template<class T>
	void BinarySearchTree<T>::clear_tree_run(BNode* ptr)
	{
		if (h != NULL)
		{
			if (ptr->left != NULL)
			{
				clear_tree_run(ptr->left);
			}

			if (ptr->right != NULL)
			{
				clear_tree_run(ptr->right);
			}
			if (ptr == h)
			{
				std::cout << "delete last node with value: " << ptr->val << " and set h pointer to NULL." << std::endl;
				h = NULL;
				BNode* delete_pointer = NULL; // create a pointer to delete a node
				delete_pointer = ptr;// set deletpointer to the BNode ptr
									 //h = ptr->left; // move h one step ahead
									 //h = h->right; // move h one step ahead
				delete delete_pointer;
			}
			else if (ptr != h)
			{
				std::cout << "delete: " << ptr->val << std::endl;
				BNode* delete_pointer = NULL; // create a pointer to delete a node
				delete_pointer = ptr;// set deletpointer to the BNode ptr
									 //h = ptr->left; // move h one step ahead
									 //h = h->right; // move h one step ahead
				delete delete_pointer;
			}	
		}
		else
		{
			std::cout << " The tree is empty! " << std::endl;
		}
	}
	template<class T>
	void BinarySearchTree<T>::clear_tree()
	{
		std::cout << " " << std::endl;
		std::cout << "Clear the tree: " << std::endl;
		clear_tree_run(h);
	}

	template<class T>
	int BinarySearchTree<T>::size_tree_run(BNode* ptr)
	{
		if (h != NULL)
		{
			if (ptr->left != NULL)
			{

				size_tree_run(ptr->left);
			}

			if (ptr->right != NULL)
			{
				
				size_tree_run(ptr->right);
			}
			size_count += 1;
			//std::cout << ptr->val << std::endl;
		}
		else
		{
			std::cout << " The tree is empty! " << std::endl;
		}
		return ptr->val;
	}

	template<class T>
	int BinarySearchTree<T>::size_tree()
	{
		int end_result;
		std::cout << " " << std::endl;
		std::cout << "Size_of_the_tree: "  <<std::endl;
		size_tree_run(h);
		//std::cout << size_tree_run(h) << std::endl;
		std::cout << size_count << std::endl;
		end_result = size_count;
		size_count = 0;
		return end_result;
	}
}

int n;
int runlist = 0; // choose what code to run, 1 for linked list, 2 for binary search tree, and 0 for both
int main()
{
	
	if (runlist == 1 || runlist == 0)
	{
		// Linked List - start
		linkedlist::LinkedList<int> list;
		list.push_back(4);
		list.push_front(1);
		list.push_back(2);
		list.push_back(3);
		list.push_back(5);
		list.push_front(6);
		list.push_back(7);
		list.push_back(3);
		list.print_list();
		verify(6, list.find_value_at_position(0), "Push_front");
		verify(3, list.find_value_at_position(7), "Push_back");
		//list.find_value_at_position(3);
		verify(2, list.find_value_at_position(3), "Find_value_at_position");
		//list.find_value(3);
		verify(3, list.find_value(3), "Find_value");
		//list.size_of_list();
		verify(8, list.size_of_list(), "Size_of_list");
		list.pop_front();
		verify_missing_in_list(6, list.find_value_at_position(0), "Pop_front");
		list.pop_back();
		verify_missing_in_list(3, list.find_value_at_position(5), "Pop_back");
		list.print_list();
		list.clear_list();
		verify_missing_in_list(3, list.find_value(7), "Clear_list");
		list.print_list();
		// Linked List - end
		std::cout  << std::endl;
		std::cout << "-----------------------------------------------------" << std::endl;
	}

	if (runlist == 2 || runlist == 0)
	{
		// Binary Search Tree - start
		binarysearchtree::BinarySearchTree<int> tree;
		tree.add_new(5);
		tree.add_new(3);
		tree.add_new(2);
		tree.add_new(4);
		tree.add_new(7);
		tree.add_new(6);
		tree.add_new(4);
		tree.add_new(1);
		tree.add_new(10);
		tree.traversal_in_order();
		tree.traversal_pre_order();
		tree.traversal_post_order();
		verify(7, tree.find_value(7), "Tree_find_value");
		//tree.find_value(7);
		verify(12, tree.find_value(12), "Tree_find_value");
		//tree.find_value(12);
		verify(8, tree.size_tree(), "Size_of_tree");
		//tree.size_tree();
		//tree.size_tree(); // k�rde en extra f�r att dubbelkolla
		tree.clear_tree();
		verify_missing_in_list(3, tree.find_value(7), "Clear_list");
		tree.traversal_in_order();
		// Binary Search Tree - end
		std::cout << "" << std::endl;
		std::cout << "-----------------------------------------------------" << std::endl;
	}
	
	std::cin >> n;
	return 0;
}
